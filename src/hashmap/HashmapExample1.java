package hashmap;

import java.util.HashMap;

public class HashmapExample1 {

    public static void main(String[] args){

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("해리", new Integer(10));
        map.put("해르미온느", new Integer(100));
        map.put("론", new Integer(85));
        map.put("드레이코", new Integer(93));
        map.put("네빌",new Integer(70));
        map.put("해르미온느", new Integer(100)); //key의 중복 허용하지 않음
        map.put(null, new Integer(50)); //key에 null을 인정
        map.put("강호동", null); //value에 null을 인정

        System.out.println(map);

        Integer num = map.get("해르미온느"); //map.get(키값)
        System.out.println("해르미온느의 성적은? "+num);
        num = map.get(null);

        System.out.println("null의 성적은? "+num);


    }

}
