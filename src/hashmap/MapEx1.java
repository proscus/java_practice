package hashmap;

import java.util.HashMap;
import java.util.Iterator;

public class MapEx1 {

    public static void main(String[] args){

        String[] msg = {"Berlin", "Paris", "Seoul", "New York", "London"};

        HashMap<Integer, String> map = new HashMap<>();

        for (int i = 0; i < msg.length; i++)
            map.put(i, msg[i]);

        System.out.println(map); //그냥 콘솔창에 출력해줘도 키값과 벨류값이 제대로 나오는 것을 확인할 수 있다.

        //map에 있는 값들을 반복해주기 위해 쓰는 클래스 Iterator
        Iterator<Integer> keys2 = map.keySet().iterator();
        while(keys2.hasNext()){
            Integer key = keys2.next();
            System.out.print("key: " + key);
            System.out.print(", value: " + map.get(key) + '\n');
        }

    }

}
