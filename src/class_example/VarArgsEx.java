package class_example;

public class VarArgsEx {

    public static void main(String[] args){
        String[] strArr = {"100","200","300"};

        System.out.println(concatenate("", "100", "200", "300", "400"));
        System.out.println(concatenate("-", strArr)); //가변인자의 본질은 어레이기 때문에, 어레이도 받는 것을 확인할 수 있다.
        System.out.println(concatenate(",", new String[]{"1", "2", "3"}));
        System.out.println(concatenate("[" + concatenate("," , new String[0]) + "]"));
        System.out.println(concatenate("]"+concatenate(","+"]")));

    }

    //가변인자 - 인자의 갯수와 상관없이 받아들임. 하지만.. 어레이를 메소드를 호출할 때마다 생성하니 효율성 부분에서 유의해야한다.
    static String concatenate(String delim, String... args){
        String result = "";

        for(String str : args){
            result += str + delim;
        }

        return result;
    }

}
