package class_example;

public class BlockTest {

    static {
        System.out.println("static {}");  //클래스 초기화 블럭
    }

    {
        System.out.println("{ }"); //인스턴스 초기화 블럭
    }

    public static void main(String[] args){

        BlockTest bt = new BlockTest();
        BlockTest bt2 = new BlockTest();

    }

}
