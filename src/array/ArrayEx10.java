package array;

import java.util.Arrays;

public class ArrayEx10 {

    //버블정렬
    public static void main(String[] args){

        int[] numArr = new int[10];

        for (int i = 0; i < numArr.length; i++) {
            System.out.print(numArr[i] = (int)(Math.random() * 10)); //궁금한점. 실제로 값이 적용되나? 그냥 콘솔창에 찍는 것인데도??? --적용돠눈 것을 확인할 수 있다.
        }
        System.out.println();

//        System.out.println(Arrays.toString(numArr)); - 적용됨.

        for (int i = 0; i < numArr.length; i++) {
            boolean changed = false; //자리바꿈이 발생했는지 확인.
            for (int j = 0; j < numArr.length - 1; j++){
                if(numArr[j] > numArr[j+1]){
                    int temp = numArr[j];
                    numArr[j] = numArr[j+1];
                    numArr[j+1] = temp;
                    changed = true;
                }
            }
            //자리바꿈이 없다면 반복문을 벗어난다. - 자리바꿈이 없다는 것은 정렬이 끝난다는 의미다.
            if(!changed) break;

            //정렬된 결과를 출력.
            for (int k=0; k<numArr.length; k++)
                System.out.print(numArr[k]);

            System.out.println();
        }

    }
}
