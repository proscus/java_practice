package array;

import java.util.Scanner;

public class MultiArrEx1 {

    public static void main(String[] args){

        //둘이 마주 앉아ㅏ 다양한 크기의 배를 상대방이 알지 못하게 배치한 다음, 번갈아가며 좌표를 불러서 상대방의 배의 위치를 알아내는 게임.

        final int SIZE = 10;
        int x = 0, y = 0;

        char[][] board = new char[SIZE][SIZE];
        byte[][] shipBoard = {
              // 1 2 3 4 5 6 7 8 9
                {0,0,0,0,0,0,1,0,0}, //1
                {1,1,1,1,0,0,1,0,0}, //2
                {0,0,0,0,0,0,1,0,0}, //3
                {0,0,0,0,0,0,1,0,0}, //4
                {0,0,0,0,0,0,0,0,0}, //5
                {1,1,0,1,0,0,0,0,0}, //6
                {0,0,0,1,0,0,0,0,0}, //7
                {0,0,0,1,0,0,0,0,0}, //8
                {0,0,0,0,0,1,1,1,0}, //9
        }; //도대체 이 다차원 배열은 왜 생성한 것일까? - 해결.

        //1행에 행번호를, 1열에 열번호를 저장한다.
        for (int i = 0; i < SIZE; i++)
            board[0][i] = board[i][0] = (char)(i+'0');

        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.printf("좌표를 입력하세요. (종료는 00)>");
            String input = scanner.nextLine();

            if(input.length() == 2){ //두 글자를 입력한 경우.
                x = input.charAt(0) - '0'; //문자를 숫자로 변환. --- char은 정수로 이루어진 유니코드로 만들어진다는 사실을 잊지말자.. 하지만 왜 빼주는건지는 잘 모르겠다.
                y = input.charAt(1) - '0';

                //종료조건
                if(x == 0 && y == 0)
                    break;

            }

            //유저가 제대로된 값을 입력하도록 강제하는 조건.
            if(input.length() != 2 || x <= 0 || x >= SIZE || y <= 0 || y >= SIZE){
                System.out.println("잘못된 입력입니다. 다시 입력해주세요.");
                continue;
            }

            //shipBoard[x-1][y-1]의 값이 1이면, '0'을 board[x][y]에 저장한다.
            board[x][y] = shipBoard[x-1][y-1]==1 ? 'O' : 'X';

           //배열 board의 내용을 화면에 출력한다. //char만 가능하다.
            for (int i = 0; i < SIZE; i++) {
                System.out.println(board[i]);
            }
            System.out.println();
        }


    }

}
