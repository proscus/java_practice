package class_example2.polyargument3;

import java.util.Vector;

class Product {

    int price; //제품의 가격
    int bonusPoint; //제품구매 시 제공하는 보너스점수

    Product(int price){
        this.price = price;
        bonusPoint = (int)(price/10.0);
    }

    Product(){}

}

class Tv extends Product {
    Tv(){
        super(100);
    }

    public String toString(){return "Tv";};
}

class Computer extends Product {
    Computer(){
        super(200);
    }

    public String toString(){return "Computer";}
}

class Audio extends Product {
    Audio(){
        super(50);
    }

    public String toString(){return "Audio";}
}

class Buyer { //고객, 물건을 사는 사람
    int money = 1000; //소유금액
    int bonusPoint = 0; //보너스점수
//    Product[] item = new Product[10]; //구입한 제품을 저장하기 위한 배열
    //배열대신 가변적인 Vector를 이용한다.
    Vector item = new Vector(); //구입한 제푸믈 저장하는데 사용될 Vector객체
    int i = 0; //Product배열에 사용될 카운터

    void buy(Product p){
        if(money < p.price){
            System.out.println("잔액이 부족하여 물건을 살 수 없습니다.");
            return;
        }

        money -= p.price;
        bonusPoint += p.bonusPoint;
        item.add(p);
        System.out.println(p + "을/를 구입하셨습니다.");
    }

    void summary(){ //구매한 물품에 대한 정보를 요약해서 보여 준다.
        int sum = 0; //구입한 물품의 가격합계
        String itemList = ""; //구입한 물품목록

        //반복문을 이용하여 구입한 물품의 총 가격과 목록을 만든다.
        for (int i = 0; i < item.size(); i++) {
            Product p = (Product) item.get(i);
            sum += p.price;
            itemList += (i==0) ? "" + p : ", " + p;
        }
        System.out.println("구입하신 물품의 총금액은 " + sum + "만원압니다.");
        System.out.println("구입하신 제품은 " + itemList + "입니다.");
    }
}

public class PolyArgumentTest3 {

    public static void main(String[] args){

        Buyer b = new Buyer();

        b.buy(new Tv());
        b.buy(new Computer());
        b.buy(new Audio());
        b.summary();

    }

}