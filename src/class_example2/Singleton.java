package class_example2;

public final class Singleton {

    //싱글톤 디자인패턴.
    private static Singleton s = new Singleton();

    private Singleton(){
        //default Constructor
    }

    public static Singleton getInstance(){
        if(s == null){
            s = new Singleton();
        }
        return s;
    }
}

class SingletonTest{

    public static void main(String args[]){

//        Singleton s = new Singleton(); //생성자가 private이므로 에러가 뜨는 것을 확인할 수 있다.
        Singleton s = Singleton.getInstance();
    }
}